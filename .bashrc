# [[file:readme.org::*.bashrc][bashrc]]
#. $HOME/.environment

#HACK as rxvt is sgid utmp and kill TMPDIR
TMPDIR=$HOME/tmp
export TMPDIR

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Turn off posix mode if it somehow got turned on.
set +o posix

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Enable the recursive wildcard **
shopt -s globstar 2>/dev/null

# Append history rather than replacing it
shopt -s histappend

# Do not complete an empty command (listing every program in $PATH)
shopt -s no_empty_cmd_completion

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

HISTSIZE=100000
unset HISTFILESIZE

# enable color support of ls and also add handy aliases so ls works the same where ever I am.
if [ -x /usr/bin/dircolors ]; then
    eval "`dircolors -b`"
    LS=$(which ls) # get ls from the path, if it's not in $PATH by this time, there are other issues.
    # if I'm on a lovely AIX box that has dircolors, I have use gnu ls.
    [ -x /opt/freeware/bin/ls ] && LS=/opt/freeware/bin/ls
    alias ls="${LS} --color=auto -v"
elif [ -x /usr/local/bin/dircolors ]; then
    eval "`/usr/local/bin/dircolors -b`"
    if [ -x /usr/local/bin/gnuls ]; then
        alias ls='/usr/local/bin/gnuls --color=auto -v'
    fi
else
    alias ls='ls -v'
fi

# For use when piping into something like less that can handle color
alias cgrep='grep --color=always'

# Make Control-v paste, if in X and if xsel available
if [ -n "$DISPLAY" ] && [ -x /usr/bin/xsel ] ; then
    # Work around a bash bug: \C-@ does not work in a key binding
    bind '"\C-x\C-m": set-mark'
    # The '#' characters ensure that kill commands have text to work on; if
    # not, this binding would malfunction at the start or end of a line.
    bind 'Control-v: "#\C-b\C-k#\C-x\C-?\"$(xsel -b -o)\"\e\C-e\C-x\C-m\C-a\C-y\C-?\C-e\C-y\ey\C-x\C-x\C-d"'
fi

# handle bash completion before setting PS1 so we can do cool things
# like add git branch to prompt....

if [ -z "$BASH_COMPLETION" ] && [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# cygwin doesn't include git-prompt.sh, so I just include it in .etc
if [ -f $HOME/.git-prompt.sh ]; then
  . $HOME/.git-prompt.sh
  export GIT_PS1_SHOWDIRTYSTATE="verbose name git"
  export GIT_PS1_SHOWSTASHSTATE="verbose name git"
  export GIT_PS1_SHOWUNTRACKEDFILES="verbose name git"
  export GIT_PS1_SHOWUPSTREAM="verbose name git"
fi

# determine if we have git completion
if [ -n "$(typeset -F __git_ps1)" ] ; then
  have_git_completion=true
fi

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# Only show username and hostname if remote or unusual
if [ -n "$SSH_CONNECTION" ] || [ "$(id -un)" != "james" ] ; then
    prompt_remote=true
fi

if [ $GUIX_ENVIRONMENT ]; then
  PS1='$(e="$?";[ "$e" -ne 0 ] && echo -n "\[\e[01;31m\]($e) ")${debian_chroot:+\[\e[01;37m\]($debian_chroot) }${prompt_remote:+\[\e[01;32m\]\u@\h\[\e[00m\]:}\[\e[01;34m\]\w${have_git_completion:+$(__git_ps1 " (%s)")}\[\e[00m\] [env]\$ '
else 
  PS1='$(e="$?";[ "$e" -ne 0 ] && echo -n "\[\e[01;31m\]($e) ")${debian_chroot:+\[\e[01;37m\]($debian_chroot) }${prompt_remote:+\[\e[01;32m\]\u@\h\[\e[00m\]:}\[\e[01;34m\]\w${have_git_completion:+$(__git_ps1 " (%s)")}\[\e[00m\] \$ '
fi 

# Set title as appropriate for terminal
case "$TERM" in
"dumb")  # fancy prompts confuse tramp.
    PS1="$ "
    ;;
*xterm*|rxvt*)
    PS1="\[\e]0;${prompt_remote:+\u@\h: }\w\a\]$PS1"
    ;;
screen)
    PS1="\[\ek${prompt_remote:+\u@\h: }\w\e"'\\'"\]$PS1"
    ;;
esac

#
#    4) Consider changing your PS1 to also show the current branch:
#        PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '
#
#       The argument to __git_ps1 will be displayed only if you
#       are currently in a git repository.  The %s token will be
#       the name of the current branch.
# bashrc ends here
export GPG_TTY=$(tty)
