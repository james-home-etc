if [ -r /usr/local/etc/env ]; then
    alias hostname=/bin/hostname
    . /usr/local/etc/env
fi
#. $HOME/.environment
# if not interactive, don't do anything
[ -z "$PS1" ] && return
set -o emacs
#let's get arrow keys to work.
alias __A=`echo "\020"` # up arrow = ^p
alias __B=`echo "\016"` # down arrow = ^n
alias __C=`echo "\006"` # right arrow = ^f
alias __D=`echo "\002"` # left arrow = ^b
setprompt ()
{
# Only show username and hostname if remote or unusual
if [ -n "$SSH_CONNECTION" ] || [ "$(id -un)" != "james" ]; then
  prompt_remote=true
  HOST=`/bin/hostname`
fi

#This seems to make the command line strange.... seems to count control chars.
#PS1='${prompt_remote:+[01;32m$USER@$HOST[0m:}[01;34m$PWD[0m \$ '
PS1='${prompt_remote:+$USER@$HOST:}$PWD \$ '
}

setprompt
#[ X"/usr/bin/ksh" == X"${SHELL}" ] && [ -x /bin/bash ] && SHELL=/bin/bash /bin/bash -l
#[ X"/usr/bin/ksh" == X"${SHELL}" ] && [ -x /bin/bash ] && sleep 5 && echo "start" && jobs && exec /bin/bash -i
