{:user {:plugins [[lein-license "0.1.6"]
                  [lein-cloverage "1.0.7-SNAPSHOT"]
                  [lein-kibit "0.1.3"]
                  [lein-bikeshed "0.4.1"]
                  [lein-ancient "0.6.10"]
                  [cider/cider-nrepl "0.12.0"]]}}

