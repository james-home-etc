
;;; My .emacs file
;;; I tried doing this with org-babel and tangling and org file, but this proved to be rather much to maintain and work consistently accross boxen.


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

(setq org-agenda-include-diary t)
;; load paths
(load "~/.emacs.d/config/load-paths")

;; local customization
(if (file-exists-p "~/.emacs.el.local")
    (load-file "~/.emacs.el.local"))

;; key-maps
;; should I put all keymaps here or put mode specific keymaps with the mode...
(load "~/.emacs.d/config/keymaps")

;;desktop
(load "~/.emacs.d/config/desktop")
;; config
(load "~/.emacs.d/config/main")
(load "~/.emacs.d/config/lisp")
(load "~/.emacs.d/config/visual")
(when (and (file-exists-p "~/Maildir") (require 'mu4e nil 'noerror))
  (load "~/.emacs.d/config/mu4e"))
(load "~/.emacs.d/config/org")
(setq org-export-backends '(beamer ascii html icalendar latex md odt))
(setq org-modules '(org-attach
                    org-checklist
                    org-contacts
                    org-crypt
                    org-habit
                    org-id))
(setq org-enforce-todo-dependencies t
      org-attach-git-annex-cutoff 1024
      org-enforce-todo-checkbox-dependencies t)
(require 'org)
(require 'blog-tools nil 'noerror)
(put 'scroll-left 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values
   (quote
    ((eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
