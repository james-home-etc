;; org mode
(require 'org)
;; required in upper level, before this file gets sourced.
;; The idea is I don't need this if I don't have my org files.
(load-library "org-compat")
;;(require 'org-install)
;;(require 'org-protocol)
;;(require 'org-publish)
(require 'org-feed)
;; I manage org files myself.
;; I really don't wish work files to end up on personal devices.
(add-hook 'org-mode-hook
          (lambda()
            (org-defkey org-mode-map "\C-c[" 'undefined)
            (org-defkey org-mode-map "\C-c]" 'undefined))
          'append)
(require 'ox-latex)
;; I want to get all org files in a directory
;; http://stackoverflow.com/questions/17215868/recursively-adding-org-files-in-a-top-level-directory-for-org-agenda-files-take

(defvar bh/hide-scheduled-and-waiting-next-tasks t)
;;(setq bh/hide-scheduled-and-waiting-next-tasks t)


(defun jr/load-org-agenda-files-recursively (dir)
  "Find all directories in DIR."
  (unless (file-directory-p dir) (error "Not a directory `%s'" dir))
  (unless (equal (directory-files dir nil org-agenda-file-regexp t) nil)
    (add-to-list 'org-agenda-files dir))
  (dolist (file (directory-files dir nil nil t))
    (unless (member file '("." ".."))
      (let ((file (concat dir file "/")))
	(when (file-directory-p file)
	  (jr/load-org-agenda-files-recursively file))))))

(when (file-directory-p "~/doc/org")
  (jr/load-org-agenda-files-recursively "~/doc/org/" )) ; trailing slash required
(when (file-directory-p "~/lib/finance")
  (jr/load-org-agenda-files-recursively "~/lib/finance/"))
;;(add-to-list 'org-agenda-files '"~/")
;;(jr/load-org-agenda-files-recursively "~/.etc/")
;;(jr/load-org-agenda-files-recursively "~/src/" ) ; trailing slash required
;;(jr/load-org-agenda-files-recursively "~/src01/" ) ; trailing slash required
(when (file-directory-p "~/doc/org")
  (setq org-directory "~/doc/org")
  (setq org-default-notes-file "~/doc/org/refile.org"))


;; (if (file-exists-p "~/doc/org")
;;     (if (boundp 'org-agenda-files)
;;         (add-to-list 'org-agenda-files '"~/doc/org")
;;       (setq org-agenda-files  '("~/doc/org"))))
;; (if (file-exists-p "~/doc/org/lowes")
;;     (add-to-list 'org-agenda-files '"~/doc/org/lowes"))
;; (if (file-exists-p "~/src/wiki/jamestechnotes.com")
;;     (add-to-list 'org-agenda-files '"~/src/wiki/jamestechnotes.com"))
;; (if (file-exists-p "~/src")
;;     (add-to-list 'org-agenda-files '"~/src"))
;; (if (file-exists-p "~/src01")
;;     (add-to-list 'org-agenda-files '"~/src01"))
;;k(setq org-mobile-files (org-agenda-files))

;; setup for MobileOrg
;;(cond ((file-exists-p "~/doc/org")
;;       (setq org-directory "~/doc/org")
;;       (setq org-mobile-directory "~/doc/MobileOrg")
;;       (setq org-mobile-inbox-for-pull "~/doc/org/MobileInbox.org"))
;;      (t
;;       (setq org-directory "~")
;;       (setq org-mobile-directory "~/tmp")
;;       (setq org-mobile-inbox-for-pull "~/tmp/MobileInbox.org")))

;;(setq org-file-apps org-file-apps-defaults-gnu)
;;(delete '("\\.pdf\\'" . default) org-file-apps)
;;(add-to-list 'org-file-apps '(("\\.pdf\\'" . "evince \"%s\"")))

;; setup todo things.
(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!/!)")
	(sequence "NEW" "IN-PROGRESS" "|" "DONE") ;; jira
	(sequence "TASK" "|" "DONE") ;; will this work for others?
        (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE")))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
	      ("NEW" :foreground "red" :weight bold)
              ("IN-PROGRESS" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("MEETING" :foreground "forest green" :weight bold)
              ("PHONE" :foreground "forest green" :weight bold)
	      ("TASK" :foreground "orange" :weight bold))))

(setq org-use-fast-todo-selection t)
(setq org-treat-S-cursor-todo-selection-as-state-change nil)

(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING" . t) ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

; Tags
(setq org-tag-alist (quote ((:startgroup)
                            ("@home" . ?H)
                            ("@lowes" . ?l)
                            (:endgroup)
                            ("PHONE" . ?p)
                            ("PERSONAL" . ?P)
                            ("crypt" . ?E)
                            ("WORK" . ?W)
                            ("NOTE" . ?n)
                            ("WIKI" . ?w)
			    ("ONE" . ?O)
			    ("TEAM" . ?T)
                            ("FLAGGED" . ??))))

(setq org-fast-tag-selection-single-key (quote expert))

; For tag searches ignore tasks with scheduled and deadline datas

(setq org-agenda-tags-todo-honor-ignore-options t)
(setq org-agenda-span 1) ;; show day to start
(setq org-deadline-warning-days 14)
(setq org-agenda-show-all-dates t)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-start-on-weekday nil)

;; setup for ics
(setq org-icalendar-use-scheduled '(event-if-todo event-if-not-todo todo-start))
(setq org-icalendar-use-deadline '(event-if-todo event-if-not-todo todo-start))
(setq org-icalendar-categories '(category todo-state all-tags))
(setq org-icalendar-store-UID t)
(setq org-icalendar-alarm-time 15)

(require 'org-contacts nil 'noerror)
(setq org-contacts-files '("~/doc/org/contacts.org"))

(setq org-capture-templates
      (quote (("t" "todo" entry (file "~/doc/org/refile.org")
               "* TODO %?\n%a\n  %i")
              ("n" "note" entry (file "~/doc/org/refile.org")
               "* %? :NOTE:\n%a\n  %i")
              ("j" "Journal" entry (file+datetree "~/doc/org/diary.org")
               "* %?\n  %i")
              ("w" "org-protocol" entry (file "~/doc/org/refile.org")
               "* TODO Review %c\n  %i")
              ("p" "Phone call" entry (file "~/doc/org/refile.org")
               "* PHONE %? :PHONE:")
	      ;; todo from email
	      ("m" "todo" entry (file+headline "~/doc/org/refile.org" "Taskss")
	       "* TODO %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n%a\n")
;              ("c" "Contacts" entry (file+headline "~/doc/org/contacts.org" "Contacts")
              ("c" "Contacts" entry (file "~/doc/org/contacts.org")
               "* %(org-contacts-template-name)
:PROPERTIES:%(org-contacts-template-email)
:Phone:
:Cell:
:Fax:
:Company:
:Street:
:City:
:State:
:Postal:
:URL:
:Notes:
:END:")
              )))

; Targets include this file and any file contributing to the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))

; Use full outline paths for refile targets - we file directly with IDO
(setq org-refile-use-outline-path t)

; Targets complete directly with IDO
;(setq org-outline-path-complete-in-steps nil) ido is broke for refile

; Allow refile to create parent tasks with confirmation
;;(setq org-refile-allow-creating-parent-nodes (quote confirm))

; Use IDO for both buffer and file completion and ido-everywhere to t
(setq org-completion-use-ido nil) ;; this is broken 9 sept 15

(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-tags-exclude-from-inheritance (quote ("crypt")))
(setq org-crypt-key "D8467AFB")

;; load EasyPG... be careful with autosave
(require 'epa-file)
(epa-file-enable)
  
;;;; Refile settings
; Exclude DONE state tasks from refile targets
(defun bh/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(setq org-refile-target-verify-function 'bh/verify-refile-target)

;;agenda
(setq org-agenda-dim-blocked-tasks t)
(setq org-agenda-compact-blocks t)

;; Custom agenda command definitions
(setq org-agenda-custom-commands
      (quote (("N" "Notes" tags "NOTE"
               ((org-agenda-overriding-header "Notes")
                (org-tags-match-list-sublevels t)))
              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-overriding-header "Habits")
                (org-agenda-sorting-strategy
                 '(todo-state-down priority-down effort-up category-keep))))
              (" " "Agenda"
               ((agenda "" nil)
                (tags "REFILE"
                      ((org-agenda-overriding-header "Tasks to Refile")
                       (org-tags-match-list-sublevels nil)))
                (tags-todo "-CANCELLED/!"
                           ((org-agenda-overriding-header "Stuck Projects")
                            (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                            (org-agenda-sorting-strategy
                             '(priority-down category-keep))))
                (tags-todo "-CANCELLED/!NEXT"
                           ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                            (org-tags-match-list-sublevels t)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(todo-state-down priority-down effort-up category-keep))))
                (tags-todo "-HOLD-CANCELLED/!"
                           ((org-agenda-overriding-header "Projects")
                            (org-agenda-skip-function 'bh/skip-non-projects)
                            (org-tags-match-list-sublevels 'indented)
                            (org-agenda-sorting-strategy
                             '(priority-down category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(priority-down category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(priority-down category-keep))))
                (tags-todo "-CANCELLED+WAITING|HOLD/!"
                           ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-tasks)
                            (org-tags-match-list-sublevels nil)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)))
                (tags "-REFILE/"
                      ((org-agenda-overriding-header "Tasks to Archive")
                       (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                       (org-tags-match-list-sublevels nil))))
               nil))))

(setq org-agenda-custom-commands1
      (quote (("N" "Notes" tags "NOTE"
               ((org-agenda-overriding-header "Notes")
                (org-tags-match-list-sublevels t)))
              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-overriding-header "Habits")
                (org-agenda-sorting-strategy
                 '(todo-state-down effort-up category-keep))))
              (" " "Agenda"
               ((agenda "" nil)
                (tags "REFILE"
                      ((org-agenda-overriding-header "Tasks to Refile")
                       (org-tags-match-list-sublevels nil)))
                (tags-todo "-CANCELLED/!"
                           ((org-agenda-overriding-header "Stuck Projects")
			    (org-agenda-skip-function 'bh/skip-non-stuck-projects)
			    (org-agenda-sorting-strategy
			     '(category-keep))))
		(tags-todo "-HOLD-CANCELLED/!"
			   ((org-agenda-overriding-header "Projects")
			    (org-agenda-skip-function 'bh/skip-non-projects)
			    (org-tags-match-list-sublevels 'indented)
			    (org-agenda-sorting-strategy
			     '(category-keep))))
		(tags-todo "-CANCELLED/!NEXT"
			   ((org-agenda-overriding-header (concat "Project Next Tasks"
								  (if bh/hide-scheduled-and-waiting-next-tasks
								      ""
								    " (including WAITING and SCHEDULED tasks)")))
			    (org-agenda-skip-function 'bh/skip-projects-and-habbits-and-single-tasks)
			    (org-tags-match-list-sublevels t)
			    (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-sorting-strategy
			     '(todo-state-down effort-up category-keep))))
		(tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
		(tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
			   ((org-agenda-overriding-header (concat "Project Subtasks"
								  (if bh/hide-scheduled-and-waiting-next-tasks
								      ""
								    " (including WAITING and SCHEDULED tasks)")))
			    (org-agenda-skip-function 'bh/skip-non-project-tasks)
			    (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-sorting-strategy
			     '(category-keep))))
		(tags-todo "-CANCELLED+WAITING|HOLD/!"
			   ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
								  (if bh/hide-scheduled-and-waiting-next-tasks
								      ""
								    " (including WAITING and SCHEDULED tasks)")))
			    (org-agenda-skip-function 'bh/skip-non-tasks)
			    (org-tags-match-list-sublevels nil)
			    (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
			    (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)))
                (tags "-REFILE/"
                      ((org-agenda-overriding-header "Tasks to Archive")
                       (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
		       (org-tags-match-list-sublevels nill)))
               nil)))))

(defun bh/org-auto-exclude-function (tag)
  "Automatic task exclusion in the agenda with / RET"
  (and (cond
        ((string= tag "hold")
         t)
        ((string= tag "farm")
         t))
       (concat "-" tag)))

(setq org-agenda-auto-exclude-function 'bh/org-auto-exclude-function)

(setq org-modules (quote (org-habit org-id)))
(setq org-habit-graph-column 50)
(setq global-auto-revert-mode t)
(setq org-crypt-disable-auto-save nil)
(setq org-stuck-projects (quote ("" nil nil "")))

(setq org-list-demote-modify-bullet (quote (("+" . "-")
                                            ("*" . "-")
                                            ("1." . "-")
                                            ("1)" . "-"))))
(setq org-tags-match-list-sublevels t)
(setq org-agenda-persistent-filter t)
(setq split-width-threshold 120)
(setq org-agenda-skip-additional-timestamps-same-entry t)
(setq org-table-use-standard-references (quote from))
(setq org-agenda-window-setup 'current-window)
(setq org-clone-delete-id t)
(setq org-cycle-include-plain-lists t)
;; make fonts come out right in html
(setq org-src-fontify-natively t)
;;(setq org-structure-template-alist
;;      (quote (("s" "#+begin_src ?\n\n#+end_src" "<src lang=\"?\">\n\n</src>")
;;              ("e" "#+begin_example\n?\n#+end_example" "<example>\n?\n</example>")
;;              ("q" "#+begin_quote\n?\n#+end_quote" "<quote>\n?\n</quote>")
;;              ("v" "#+begin_verse\n?\n#+end_verse" "<verse>\n?\n/verse>")
;;              ("c" "#+begin_center\n?\n#+end_center" "<center>\n?\n/center>")
;;              ("l" "#+begin_latex\n?\n#+end_latex" "<literal style=\"latex\">\n?\n</literal>")
;;              ("L" "#+latex: " "<literal style=\"latex\">?</literal>")
;;              ("h" "#+begin_html\n?\n#+end_html" "<literal style=\"html\">\n?\n</literal>")
;;              ("H" "#+html: " "<literal style=\"html\">?</literal>")
;;              ("a" "#+begin_ascii\n?\n#+end_ascii")
;; ;;             ("A" "#+ascii: ")
;;              ("i" "#+index: ?" "#+index: ?")
;;              ("I" "#+include %file ?" "<include file=%file markup=\"?\">"))))

(setq org-startup-indented t
      org-export-headline-levels 10)
(defun bh/mark-next-parent-tasks-todo ()
  "Visit each parent task and change NEXT states to TODO"
  (let ((mystate (or (and (fboundp 'state)
                          state)
                     (nth 2 (org-heading-components)))))
    (when (equal mystate "NEXT")
      (save-excursion
        (while (org-up-heading-safe)
          (when (member (nth 2 (org-heading-components)) (list "NEXT"))
            (org-todo "TODO")))))))

(add-hook 'org-after-todo-state-change-hook 'bh/mark-next-parent-tasks-todo 'append)
(add-hook 'org-clock-in-hook 'bh/mark-next-parent-tasks-todo 'append)
(run-at-time "00:59" 3600 'org-save-all-org-buffers)

(defun bh/find-project-task ()
  "Move point to the parent (project) task if any"
  (save-restriction
    (widen)
    (let ((parent-task (save-excursion (org-back-to-heading 'invisible-ok) (point))))
      (while (org-up-heading-safe)
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq parent-task (point))))
      (goto-char parent-task)
      parent-task)))

(defun bh/is-project-p ()
  "Any task with a todo keyword subtask"
  (save-restriction
    (widen)
    (let ((has-subtask)
          (subtree-end (save-excursion (org-end-of-subtree t)))
          (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
      (save-excursion
        (forward-line 1)
        (while (and (not has-subtask)
                    (< (point) subtree-end)
                    (re-search-forward "^\*+ " subtree-end t))
          (when (member (org-get-todo-state) org-todo-keywords-1)
            (setq has-subtask t))))
      (and is-a-task has-subtask))))

(defun bh/is-project-subtree-p ()
  "Any task with a todo keyword that is in a project subtree.
Callers of this function already widen the buffer view."
  (let ((task (save-excursion (org-back-to-heading 'invisible-ok)
                              (point))))
    (save-excursion
      (bh/find-project-task)
      (if (equal (point) task)
          nil
        t))))

(defun bh/is-task-p ()
  "Any task with a todo keyword and no subtask"
  (save-restriction
    (widen)
    (let ((has-subtask)
          (subtree-end (save-excursion (org-end-of-subtree t)))
          (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
      (save-excursion
        (forward-line 1)
        (while (and (not has-subtask)
                    (< (point) subtree-end)
                    (re-search-forward "^\*+" subtree-end t))
          (when (member (org-get-todo-state) org-todo-keywords-1)
            (setq has-subtask t))))
      (and is-a-task (not has-subtask)))))

(defun bh/is-subproject-p ()
  "Any task with is a subtask of another project"
  (let ((is-subproject)
        (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
    (save-excursion
      (while (and (not is-subproject) (org-up-heading-safe))
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq is-subproject t))))
    (and is-a-task is-subproject)))

(defun bh/list-sublevels-for-projects-indented ()
  "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
  (if (marker-buffer org-agenda-restrict-begin)
      (setq org-tags-match-list-sublevels 'indented)
    (setq org-tags-match-list-sublevels nil))
  nil)

(defun bh/list-sublevels-for-projects ()
  "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
  (if (marker-buffer org-agenda-restrict-begin)
      (setq org-tags-match-list-sublevels t)
    (setq org-tags-match-list-sublevels nil))
  nil)


(defun bh/toggle-next-task-display ()
  (interactive)
  (setq bh/hide-scheduled-and-waiting-next-tasks (not bh/hide-scheduled-and-waiting-next-tasks))
  (when  (equal major-mode 'org-agenda-mode)
    (org-agenda-redo))
  (message "%s WAITING and SCHEDULED NEXT Tasks" (if bh/hide-scheduled-and-waiting-next-tasks "Hide" "Show")))

(defun bh/skip-stuck-projects ()
  "Skip trees that are not stuck projects"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (if (bh/is-project-p)
          (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
                 (has-next ))
            (save-excursion
              (forward-line 1)
              (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
                (unless (member "WAITING" (org-get-tags-at))
                  (setq has-next t))))
            (if has-next
                nil
              next-headline)) ; a stuck project, has subtasks but no next task
        nil))))

(defun bh/skip-non-stuck-projects ()
  "Skip trees that are not stuck projects"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (if (bh/is-project-p)
          (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
		 (has-next ))
	    (save-excursion
	      (forward-line 1)
	      (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
		(unless (member "WAITING" (org-get-tags-at))
		  (setq has-next t))))
            (if has-next
                next-headline
              nil)) ; a stuck project, has subtasks but no next task
        next-headline))))

(defun bh/skip-non-projects ()
  "Skip trees that are not projects"
  ;;(bh/list-sublevels-for-projects-indented)
  (if (save-excursion (bh/skip-non-stuck-projects))
      (save-restriction
        (widen)
        (let ((subtree-end (save-excursion (org-end-of-subtree t))))
          (cond
	   ((bh/is-project-p)
	    nil)
	   ((and (bh/is-project-subtree-p) (not (bh/is-task-p)))
	    nil)
           (t
	    subtree-end))))
    (save-excursion (org-end-of-subtree t))))

(defun bh/skip-non-tasks ()
  "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((bh/is-task-p)
        nil)
       (t
        next-headline)))))

(defun bh/skip-project-trees-and-habits ()
  "Skip trees that are projects"
  (save-restriction
    (widen)
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((org-is-habit-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-projects-and-habits-and-single-tasks ()
  "Skip trees that are projects, tasks that are habits, single non-project tasks"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((org-is-habit-p)
        next-headline)
       ((and bh/hide-scheduled-and-waiting-next-tasks
	     (member "WAITING" (org-get-tags-at)))
        next-headline)
       ((bh/is-project-p)
	next-headline)
       ((and (bh/is-task-p) (not (bh/is-project-subtree-p)))
        next-headline)
       (t
        nil)))))

(defun bh/skip-project-tasks-maybe ()
  "Show tasks related to the current restriction.
When restricted to a project, skip project and sub project tasks, habits, NEXT tasks, and loose tasks.
When not restricted, skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           (next-headline (save-excursion (or (outline-next-heading) (point-max))))
           (limit-to-project (marker-buffer org-agenda-restrict-begin)))
      (cond
       ((bh/is-project-p)
        next-headline)
       ((org-is-habit-p)
        subtree-end)
       ((and (not limit-to-project)
             (bh/is-project-subtree-p))
        subtree-end)
       ((and limit-to-project
             (bh/is-project-subtree-p)
             (member (org-get-todo-state) (list "NEXT")))
        subtree-end)
       (t
        nil)))))

(defun bh/skip-project-tasks ()
  "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((org-is-habit-p)
        subtree-end)
       ((bh/is-project-subtree-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-non-project-tasks ()
  "Show project tasks.
Skip project and sub-project tasks, habits, and loose non-project tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           (next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((bh/is-project-p)
        next-headline)
       ((org-is-habit-p)
        subtree-end)
       ((and (bh/is-project-subtree-p)
             (member (org-get-todo-state) (list "NEXT")))
        subtree-end)
       ((not (bh/is-project-subtree-p))
        subtree-end)
       (t
        nil)))))

(defun bh/skip-projects-and-habits ()
  "Skip trees that are projects and tasks that are habits"
  (save-restriction
    (widen)
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((org-is-habit-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-non-subprojects ()
  "Skip trees that are not projects"
  (let ((next-headline (save-excursion (outline-next-heading))))
    (if (bh/is-subproject-p)
        nil
      next-headline)))

(setq org-archive-mark-done nil)
(setq org-archive-location "%s_archive::* Archived Tasks")

(defun bh/skip-non-archivable-tasks ()
  "Skip trees that are not available for archiving"
  (save-restriction
    (widen)
    ;; Consider only tasks with done todo headings as archivable candidates
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max))))
	  (subtree-end (save-excursion (org-end-of-subtree t))))
      (if (member (org-get-todo-state) org-todo-keywords-1)
	  (if (member (org-get-todo-state) org-done-keywords)
	      (let* ((daynr (string-to-number (format-time-string "%d" (current-time))))
		     (a-month-ago (* 60 60 24 (+ daynr 1)))
		     (last-month (format-time-string "%Y-%m-" (time-subtract (current-time) (seconds-to-time a-month-ago))))
		     (this-month (format-time-string "%Y-%m-" (current-time)))
		     (subtree-is-current (save-excursion
					   (forward-line 1)
					   (and (< (point) subtree-end)
						(re-search-forward (concat last-month "\\|" this-month) subtree-end t)))))
		(if subtree-is-current
		    next-headline ; Has a date in this month or last month, skip it
		  nil))  ; available to archive
	    (or subtree-end (point-max)))
	next-headline))))

(setq org-alphabetical-lists t)

; Erase all reminders and rebuilt reminders for today from the agenda
(defun bh/org-agenda-to-appt ()
  (interactive)
  (setq appt-time-msg-list nil)
  (org-agenda-to-appt))

; Rebuild the reminders everytime the agenda is displayed
(add-hook 'org-finalize-agenda-hook 'bh/org-agenda-to-appt 'append)

; This is at the end of my .emacs - so appointments are set up when Emacs starts
(bh/org-agenda-to-appt)

; Activate appointments so we get notifications
(appt-activate t)

; If we leave Emacs running overnight - reset the appointments one minute after midnight
(run-at-time "24:01" nil 'bh/org-agenda-to-appt)


;; Keep tasks with dates on the global todo lists
(setq org-agenda-todo-ignore-with-date nil)

;; Keep tasks with deadlines on the global todo lists
(setq org-agenda-todo-ignore-deadlines nil)

;; Keep tasks with scheduled dates on the global todo lists
(setq org-agenda-todo-ignore-scheduled nil)

;; Keep tasks with timestamps on the global todo lists
(setq org-agenda-todo-ignore-timestamp nil)

;; Remove completed deadline tasks from the agenda view
(setq org-agenda-skip-deadline-if-done t)

;; Remove completed scheduled tasks from the agenda view
(setq org-agenda-skip-scheduled-if-done t)

;; Remove completed items from search results
(setq org-agenda-skip-timestamp-if-done t)


(setq org-agenda-include-diary nil)
(setq org-agenda-diary-file "~/doc/org/diary.org")
(setq org-agenda-insert-diary-extract-time t)
(setq org-agenda-text-search-extra-files (quote (agenda-archives)))
(setq org-agenda-repeating-timestamp-show-all t)

;; Show all agenda dates - even if they are empty
(setq org-agenda-show-all-dates t)

;; let's try priorities
;; org defaults to a b c, with b being the default.
;; let's try a b c d e f g h i, or am I being silly.
;; high is a b c, medium is d e f, low is g h i
(setq org-enable-priority-commands t
      org-default-priority ?E
      org-lowest-priority ?I)


;; Sorting order for tasks on the agenda
(setq org-agenda-sorting-strategy
      (quote ((agenda habit-down time-up user-defined-up priority-down effort-up category-keep)
              (todo category-up priority-down effort-up)
              (tags category-up priority-down effort-up)
              (search category-up))))

;; Start the weekly agenda on Monday
;;(setq org-agenda-start-on-weekday 1)

;; Enable display of the time grid so we can see the marker for the current time
(setq org-agenda-time-grid '((daily today remove-match)
			      (0 300 600 800 1000 1200 1300 1500 1700 1900 2100 2300 2400)
			      "......" "----------------"))

;; Display tags farther right
(setq org-agenda-tags-column -102)

;;
;; Agenda sorting functions
;;
(setq org-agenda-cmp-user-defined 'bh/agenda-sort)

(defun bh/agenda-sort (a b)
  "Sorting strategy for agenda items.
Late deadlines first, then scheduled, then non-late deadlines"
  (let (result num-a num-b)
    (cond
     ; time specific items are already sorted first by org-agenda-sorting-strategy

     ; non-deadline and non-scheduled items next
     ((bh/agenda-sort-test 'bh/is-not-scheduled-or-deadline a b))

     ; deadlines for today next
     ((bh/agenda-sort-test 'bh/is-due-deadline a b))

     ; late deadlines next
     ((bh/agenda-sort-test-num 'bh/is-late-deadline '< a b))

     ; scheduled items for today next
     ((bh/agenda-sort-test 'bh/is-scheduled-today a b))

     ; late scheduled items next
     ((bh/agenda-sort-test-num 'bh/is-scheduled-late '> a b))

     ; pending deadlines last
     ((bh/agenda-sort-test-num 'bh/is-pending-deadline '< a b))

     ; finally default to unsorted
     (t (setq result nil)))
    result))

(defmacro bh/agenda-sort-test (fn a b)
  "Test for agenda sort"
  `(cond
    ; if both match leave them unsorted
    ((and (apply ,fn (list ,a))
          (apply ,fn (list ,b)))
     (setq result nil))
    ; if a matches put a first
    ((apply ,fn (list ,a))
     (setq result -1))
    ; otherwise if b matches put b first
    ((apply ,fn (list ,b))
     (setq result 1))
    ; if none match leave them unsorted
    (t nil)))

(defmacro bh/agenda-sort-test-num (fn compfn a b)
  `(cond
    ((apply ,fn (list ,a))
     (setq num-a (string-to-number (match-string 1 ,a)))
     (if (apply ,fn (list ,b))
         (progn
           (setq num-b (string-to-number (match-string 1 ,b)))
           (setq result (if (apply ,compfn (list num-a num-b))
                            -1
                          1)))
       (setq result -1)))
    ((apply ,fn (list ,b))
     (setq result 1))
    (t nil)))

(defun bh/is-not-scheduled-or-deadline (date-str)
  (and (not (bh/is-deadline date-str))
       (not (bh/is-scheduled date-str))))

(defun bh/is-due-deadline (date-str)
  (string-match "Deadline:" date-str))

(defun bh/is-late-deadline (date-str)
  (string-match "In *\\(-.*\\)d\.:" date-str))

(defun bh/is-pending-deadline (date-str)
  (string-match "In \\([^-]*\\)d\.:" date-str))

(defun bh/is-deadline (date-str)
  (or (bh/is-due-deadline date-str)
      (bh/is-late-deadline date-str)
      (bh/is-pending-deadline date-str)))

(defun bh/is-scheduled (date-str)
  (or (bh/is-scheduled-today date-str)
      (bh/is-scheduled-late date-str)))

(defun bh/is-scheduled-today (date-str)
  (string-match "Scheduled:" date-str))

(defun bh/is-scheduled-late (date-str)
  (string-match "Sched\.\\(.*\\)x:" date-str))

(require 'org-git-link nil 'noerror)
(require 'org-checklist nil 'noerror)
(setq org-enforce-todo-dependencies t)
(setq org-hide-leading-stargs nil)
(setq org-startup-indented t)
(setq org-cycle-separator-lines 2)
(setq org-blank-before-new-entry (quote ((heading)
                                         (plain-list-item . auto))))
(setq org-insert-heading-respect-content nil)
(setq org-reverse-note-order nil)
(setq org-show-following-heading t)
(setq org-show-hierarchy-above t)
(setq org-show-siblings (quote ((default))))
(setq org-deadline-warning-days 30)
(setq org-log-done (quote time))
(setq org-log-into-drawer "LOGBOOK")


(setq org-ditaa-jar-path "~/java/ditaa0_6b.jar")
(setq org-plantuml-jar-path "/usr/share/plantuml/plantuml.jar")

(add-hook 'org-babel-after-execute-hook 'bh/display-inline-images 'append)

; Make babel results blocks lowercase
(setq org-babel-results-keyword "results")

(defun bh/display-inline-images ()
  (condition-case nil
      (org-display-inline-images)
    (error nil)))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
         (dot . t)
         (ditaa . t)
         (R . t)
         (python . t)
         (ruby . t)
         (gnuplot . t)
         (clojure . t)
         ;;(sh . t)
         (ledger . t)
         (org . t)
         (plantuml . t)
         (latex . t)
         (makefile . t)))

; Do not prompt to confirm evaluation
; This may be dangerous - make sure you understand the consequences
; of setting this -- see the docstring for details
(setq org-confirm-babel-evaluate nil)

; Cache all babel results blocks by default
; For graphics generation this is way faster if nothing changes
(setq org-babel-default-header-args
     (cons '(:cache . "yes")
     (assq-delete-all :cache org-babel-default-header-args)))

; Use fundamental mode when editing plantuml blocks with C-c '
(add-to-list 'org-src-lang-modes (quote ("plantuml" . fundamental)))

; publishing

; experimenting with docbook exports - not finished
(setq org-docbook-xsl-fo-proc-command "fop %s %s")
(setq org-docbook-xslt-proc-command "xsltproc --output %s /usr/share/xml/docbook/stylesheet/nwalsh/fo/docbook.xsl %s")
;
; Inline images in HTML instead of producting links to the image
(setq org-html-inline-images t)
; Do not use sub or superscripts - I currently don't need this functionality in my documents
;(setq org-export-with-sub-superscripts nil)
; Use org.css from the norang website for export document stylesheets
;;(setq org-export-html-style-extra "<link rel=\"stylesheet\" href=\"http://doc.norang.ca/org.css\" type=\"text/css\" />")
(setq org-html-style-include-default nil)
; Do not generate internal css formatting for HTML exports
(setq org-htmlize-output-type (quote inline-css))
; Export with LaTeX fragments
(setq org-with-LaTeX-fragments t)
; I wish to do abstracts nad lists...
(defun org-latex-no-toc (depth)
  (when depth
    (format "%% Org-mode is exporting headings to %s levels.\n"
            depth)))
;;(setq org-latex-format-toc-function 'org-latex-no-toc)
;; now part of org-mode (require 'org-special-blocks)

					; use minted
(require 'ox-latex)
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("frame" "single")
        ("fontsize" "\\scriptsize")
	;("xleftmargin" "50pt")
	;("xrightmargin" "50pt")
        ("linenos" "true")))
;; cywin doesn't have texi2dvi
(if (eq system-type 'gnu/linux)
    (setq org-latex-pdf-process '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
				  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
				  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  (setq org-latex-pdf-process '("texi2dvi --pdf --tidy --shell-escape --batch %f")))
; List of projects
; norang       - http://www.norang.ca/
; doc          - http://doc.norang.ca/
; org-mode-doc - http://doc.norang.ca/org-mode.html and associated files
; org          - miscellaneous todo lists for publishing
(setq org-publish-project-alist
      ;
      ; http://www.norang.ca/  (norang website)
      ; norang-org are the org-files that generate the content
      ; norang-extra are images and css files that need to be included
      ; norang is the top-level project that gets published
      (quote (("norang-org"
               :base-directory "~/git/www.norang.ca"
               :publishing-directory "/ssh:www-data@www:~/www.norang.ca/htdocs"
               :recursive t
               :table-of-contents nil
               :base-extension "org"
               :publishing-function org-publish-org-to-html
               :style-include-default nil
               :section-numbers nil
               :table-of-contents nil
               :style "<link rel=\"stylesheet\" href=\"norang.css\" type=\"text/css\" />"
               :author-info nil
               :creator-info nil)
              ("norang-extra"
               :base-directory "~/git/www.norang.ca/"
               :publishing-directory "/ssh:www-data@www:~/www.norang.ca/htdocs"
               :base-extension "css\\|pdf\\|png\\|jpg\\|gif"
               :publishing-function org-publish-attachment
               :recursive t
               :author nil)
              ("norang"
               :components ("norang-org" "norang-extra"))
              ;
              ; http://doc.norang.ca/  (norang website)
              ; doc-org are the org-files that generate the content
              ; doc-extra are images and css files that need to be included
              ; doc is the top-level project that gets published
              ("doc-org"
               :base-directory "~/git/doc.norang.ca/"
               :publishing-directory "/ssh:www-data@www:~/doc.norang.ca/htdocs"
               :recursive nil
               :section-numbers nil
               :table-of-contents nil
               :base-extension "org"
               :publishing-function (org-publish-org-to-html org-publish-org-to-org)
               :style-include-default nil
               :style "<link rel=\"stylesheet\" href=\"/org.css\" type=\"text/css\" />"
               :author-info nil
               :creator-info nil)
              ("doc-extra"
               :base-directory "~/git/doc.norang.ca/"
               :publishing-directory "/ssh:www-data@www:~/doc.norang.ca/htdocs"
               :base-extension "css\\|pdf\\|png\\|jpg\\|gif"
               :publishing-function org-publish-attachment
               :recursive nil
               :author nil)
              ("doc"
               :components ("doc-org" "doc-extra"))
              ("doc-private-org"
               :base-directory "~/git/doc.norang.ca/private"
               :publishing-directory "/ssh:www-data@www:~/doc.norang.ca/htdocs/private"
               :recursive nil
               :section-numbers nil
               :table-of-contents nil
               :base-extension "org"
               :publishing-function (org-publish-org-to-html org-publish-org-to-org)
               :style-include-default nil
               :style "<link rel=\"stylesheet\" href=\"/org.css\" type=\"text/css\" />"
               :auto-sitemap t
               :sitemap-filename "index.html"
               :sitemap-title "Norang Private Documents"
               :sitemap-style "tree"
               :author-info nil
               :creator-info nil)
              ("doc-private-extra"
               :base-directory "~/git/doc.norang.ca/private"
               :publishing-directory "/ssh:www-data@www:~/doc.norang.ca/htdocs/private"
               :base-extension "css\\|pdf\\|png\\|jpg\\|gif"
               :publishing-function org-publish-attachment
               :recursive nil
               :author nil)
              ("doc-private"
               :components ("doc-private-org" "doc-private-extra"))
              ;
              ; Miscellaneous pages for other websites
              ; org are the org-files that generate the content
              ("org-org"
               :base-directory "~/git/org/"
               :publishing-directory "/ssh:www-data@www:~/org"
               :recursive t
               :section-numbers nil
               :table-of-contents nil
               :base-extension "org"
               :publishing-function org-publish-org-to-html
               :style-include-default nil
               :style "<link rel=\"stylesheet\" href=\"/org.css\" type=\"text/css\" />"
               :author-info nil
               :creator-info nil)
              ;
              ; http://doc.norang.ca/  (norang website)
              ; org-mode-doc-org this document
              ; org-mode-doc-extra are images and css files that need to be included
              ; org-mode-doc is the top-level project that gets published
              ; This uses the same target directory as the 'doc' project
              ("org-mode-doc-org"
               :base-directory "~/git/org-mode-doc/"
               :publishing-directory "/ssh:www-data@www:~/doc.norang.ca/htdocs"
               :recursive t
               :section-numbers nil
               :table-of-contents nil
               :base-extension "org"
               :publishing-function (org-publish-org-to-html org-publish-org-to-org)
               :plain-source t
               :htmlized-source t
               :style-include-default nil
               :style "<link rel=\"stylesheet\" href=\"/org.css\" type=\"text/css\" />"
               :author-info nil
               :creator-info nil)
              ("org-mode-doc-extra"
               :base-directory "~/git/org-mode-doc/"
               :publishing-directory "/ssh:www-data@www:~/doc.norang.ca/htdocs"
               :base-extension "css\\|pdf\\|png\\|jpg\\|gif"
               :publishing-function org-publish-attachment
               :recursive t
               :author nil)
              ("org-mode-doc"
               :components ("org-mode-doc-org" "org-mode-doc-extra"))
              ;
              ; http://doc.norang.ca/  (norang website)
              ; org-mode-doc-org this document
              ; org-mode-doc-extra are images and css files that need to be included
              ; org-mode-doc is the top-level project that gets published
              ; This uses the same target directory as the 'doc' project
              ("tmp-org"
               :base-directory "/tmp/publish/"
               :publishing-directory "/ssh:www-data@www:~/www.norang.ca/htdocs/tmp"
               :recursive t
               :section-numbers nil
               :table-of-contents nil
               :base-extension "org"
               :publishing-function (org-publish-org-to-html org-publish-org-to-org)
               :style "<link rel=\"stylesheet\" href=\"http://doc.norang.ca/org.css\" type=\"text/css\" />"
               :plain-source t
               :htmlized-source t
               :style-include-default nil
               :auto-sitemap t
               :sitemap-filename "index.html"
               :sitemap-title "Test Publishing Area"
               :sitemap-style "tree"
               :author-info t
               :creator-info t)
              ("tmp-extra"
               :base-directory "/tmp/publish/"
               :publishing-directory "/ssh:www-data@www:~/www.norang.ca/htdocs/tmp"
               :base-extension "css\\|pdf\\|png\\|jpg\\|gif"
               :publishing-function org-publish-attachment
               :recursive t
               :author nil)
              ("tmp"
               :components ("tmp-org" "tmp-extra")))))

(setq org-publish-project-alist
      '(
        ("doc-jtn-com-private"
         :base-directory "~/doc/doc.jamestechnotes.com-private"
         :base-extension "org"
         :publishing-directory "/ssh:chara.dreamhost.com:~/doc.jamestechnotes.com/private"
         :recursive t
         :publishing-functino org-publish-org-to-html
         :headline-levels 6
         :auto-preamble t
         )
        ("doc-jtn" :components ("doc-jtn-com-private"))
        ("jamestechnotes-com"
         :base-directory "~/src/wiki/jamestechnotes.com"
         :base-extension "org"
         :publishing-directory "~/public_html/"
         :recursive t
         :publishing-function (org-publish-org-to-html org-publish-org-to-org)
         :headline-levels 6
         :auto-preamble t
         :auto-sitemap nil
         :makeindex nil
         :style "<meta name=\"author\" content=\"James Richardson\" /><meta name=\"authorurl\" content=\"http://jamestechnotes.com\" /><link href=\"http://www.myopenid.com/server\" rel=\"openid.server\" /><link href=\"http://www.myopenid.com/server\" rel=\"openid2.provider\" /><link href=\"http://james-richardson-jr.myopenid.com/\" rel=\"openid.delegate\" /><link href=\"http://james-richardson-jr.myopenid.com/\" rel=\"openid2.local_id\" /><link href=\"https://plus.google.com/100087072360376597478\" rel=\"publisher\" /><link href=\"http://jamestechnotes.com\" rel=\"author\"/> <link rel=\"stylesheet\" title=\"Standard\" href=\"/css/org.css\" type=\"text/css\"/>"
         :html-preamble "<div id=\"menu\"><a href=\"index.html\">Home</a> | <a href=\"about.html\">About</a></div>"
         )
        ("jamestechnotes-com-static"
         :base-directory "~/src/wiki/jamestechnotes.com"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|ogg\\|swf\\|txt"
         :publishing-directory "~/public_html"
         :recursive t
         :publishing-function org-publish-attachment
         )
        ("jamestechnotes" :components ("jamestechnotes-com" "jamestechnotes-com-static"))
        ))
; I'm lazy and don't want to remember the name of the project to publish when I modify
; a file that is part of a project.  So this function saves the file, and publishes
; the project that includes this file
;
; It's bound to C-S-F12 so I just edit and hit C-S-F12 when I'm done and move on to the next thing
(defun bh/save-then-publish ()
  (interactive)
  (save-buffer)
  (org-save-all-org-buffers)
  (org-publish-current-project))

(global-set-key (kbd "C-s-<f12>") 'bh/save-then-publish)

;; push
;;(defvar org-mobile-push-timer nil
;;  "Timer that `org-mobile-push-timer' uses to reschedule itself, or nil")

;;(defun org-mobile-push-with-delay (secs)
;;  (when org-mobile-push-timer
;;    (cancel-timer org-mobile-push-timer))
;;  (setq org-mobile-push-timer
;;	(run-with-idle-timer
;;	 (* 1 secs) nil 'org-mobile-push)))

;; (add-hook 'after-save-hook
;; 	  (lambda()
;; 	    (when (eq major-mode 'org-mode)
;; 	      (dolist (file (org-mobile-files-alist))
;; 		(if (string= (expand-file-name (car file)) (buffer-file-name))
;; 		    (org-mobile-push-with-delay 30))))))

;;(run-at-time "00:05" 86400 '(lambda () (org-mobile-push-with-delay 1)))

;; pul
;;(org-mobile-pull)

;;(defun install-monitor (file secs)
;;  (run-with-timer 0 secs
;;    (lambda (f p)
;;      (unless (< p (second (time-since (elt (file-attributes f) 5))))
;;        (org-mobile-pull)))
;;    file secs))

;; (install-monitor (file-truename
;;                   (concat
;;                    (file-name-as-directory org-mobile-directory)
;;                    org-mobile-capture-file))
;;                  5)

;;(run-with-timer 0 (* 60 60) 'org-mobile-pull) ; pull every hour


;; ;; export org mode buffer as html w/o header crap
;; (global-set-key (kbd "<f9> x") 'jr/connections)
;; (defun jr/connections (arg)
;;   (interactive "P")
;;   (org-as-html arg nil nil t))

;; rss feeds
;; (setq org-feed-alist
;;       '(("Armed and Dangerous"
;; 	 "http://esr.ibiblio.org/?feed=rss2"
;; 	 "~/doc/org/todo.org" "catb")
;; 	("Git Annex"
;; 	 "https://git-annex.branchable.com/forum/index.rss"
;; 	 "~/doc/org/todo.org" "gitannex")
;; 	("Ikiwki"
;; 	 "http://ikiwiki.info/news/index.rss"
;; 	 "~/doc/org/todo.org" "ikiwiki")
;; 	("myglpus"
;; 	 "http://gplusrss.com/rss/feed/2eec32c7a808ca3556325d3160b5ebd3546e7948ad7f8"
;; 	 "~/doc/org/todo.org" "mygplus")
;; 	("bofh"
;; 	 "http://www.theregister.co.uk/data_centre/bofh/headlines.rss"
;; 	 "~/doc/org/todo.org" "BOFH")
;; 	("reg-servers"
;; 	 "http://www.theregister.co.uk/data_centre/servers/headlines.rss"
;; 	 "~/doc/org/todo.org" "reg-servers")
;; 	("reg-os"
;; 	 "http://www.theregister.co.uk/software/os/headlines.rss"
;; 	 "~/doc/org/todo.org" "reg-os")
;; 	("reg-apps"
;; 	 "http://www.theregister.co.uk/software/apps/headlines.rss"
;; 	 "~/doc/org/todo.org" "reg-apps")
;; 	("reg-dev"
;; 	 "http://www.theregister.co.uk/software/developer/headlines.rss"
;; 	 "~/doc/org/todo.org" "reg-dev")
;; 	("reg-stob"
;; 	 "http://www.theregister.co.uk/software/stob/headlines.rss"
;; 	 "~/doc/org/todo.org" "reg-stob")
;; 	("reg-sci"
;; 	 "http://www.theregister.co.uk/science/special_projects_bureau/headlines.rss"
;; 	 "~/doc/org/todo.org" "reg-sci")
;; 	("sci-american"
;; 	 "http://rss.sciam.com/sciam/topic/quantum-physics"
;; 	 "~/doc/org/todo.org" "sci-american")
;; 	("roku"
;; 	 "http://blog.roku.com/feed/"
;; 	 "~/doc/org/todo.org" "roku")
;; 	("xkcd"
;; 	 "http://xkcd.com/rss.xml"
;; 	 "~/doc/org/todo.org" "xkcd")
;; 	("dilbert"
;; 	 "http://www.comicsyndicate.org/Feed/Dilbert"
;; 	 "~/doc/org/todo.org" "dilbert")
;; 	("bloom"
;; 	 "http://www.comicsyndicate.org/Feed/Bloom%20County"
;; 	 "~/doc/org/todo.org" "bloom")
;; 	("rush"
;; 	 "http://feeds.feedburner.com/rushLimbaugh-AllContent"
;; 	 "~/doc/org/todo.org" "rush")))
(setq org-feed-default-template
      "\n* %h\n  SCHEDULED: %T\n  %description\n  %a\n")

;; setup solar
(setq calendar-latitude 35.6
      calendar-longitude -80.8
      calendar-location-name "Mooresville, NC"
      calendar-time-zone -300
      calendar-standard-time-zone-name "EST"
      calendar-daylight-time-zone-name "EDT")

;; org-weather
(when (require 'org-weather nil 'noerror)
  (setq org-weather-location "4480125"
	org-weather-api-url "http://api.openweathermap.org/data/2.5/forecast/daily?id=%s&mode=json&units=imperial&cnt=7&APPID=%s"
	org-weather-temperature-unit "°F"
	org-weather-speed-unit "mph")
  (org-weather-refresh))

;; effort and clocking
; Set default column view headings: Task Effort Clock_Summary
(setq org-columns-default-format "%80ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM")

; global Effort estimate values
; global STYLE property values for completion
(setq org-global-properties (quote (("Effort_ALL" . "0:15 0:30 0:45 1:00 2:00 3:00 4:00 5:00 6:00 8:00")
                                    ("STYLE_ALL" . "habit"))))


;; org-jira
;; set defcustom before loading
(setq jiralib-url "https://tools.lowes.com/jira"
      ;;jiralib-wsdl-descriptor-url "https://tools.lowes.com/jira/rpc/soap/jirasoapservice-v2?wsdl"
      org-jira-working-dir "~/doc/org/lowes"
      org-jira-use-status-as-todo t)
(require 'org-jira nil 'noerror)
(setq request-message-level 'debug
      request-log-level 'debug)

(defun jr/insert-latex-boilerplate ()
    "Insert boilerplate into current buffer"
  (interactive)
  (insert "#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER:
#+LATEX_HEADER_EXTRA:
#+DESCRIPTION:
#+KEYWORDS:
#+SUBTITLE:
#+LATEX_COMPILER: pdflatex
#+DATE: \\today

#+OPTIONS: ':nil *:t -:t ::t <:t H:10 \\n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not \"LOGBOOK\") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:nil todo:t |:t
#+TITLE: set this
#+AUTHOR: James Richardson
#+EMAIL: james.richardson@lowes.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 25.1.1 (Org mode 9.0.5)

#+LaTeX: \\thispagestyle{empty}

#+BEGIN_abstract
#+END_abstract

#+LaTeX: \\newpage
#+LaTex: \\twocolumn
#+LaTeX: \\pagestyle{plain}
#+LaTeX: \\setcounter{page}{1}
#+LaTeX: \\pagenumbering{roman}

#+LATEX: \\tableofcontents
#+LATEX: \\listoftables
#+LATEX: \\listoffigures
#+LATEX: \\listoflistings
#+LaTeX: \\onecolumn
#+LaTeX: \\pagestyle{headings}
#+LaTeX: \\setcounter{page}{1}
#+LaTeX: \\pagenumbering{arabic}

Text here

#+LaTeX: \\newpage
#+LaTeX: \\appendix

Appendices here"))
