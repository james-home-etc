(require 'org-mu4e)
(require 'mu4e-contrib)
(setq mail-user-agent 'mu4e-user-agent)
(setq message-send-mail-function 'smtpmail-send-it)
;(setq message-sendmail-f-is-evil 't)
(setq message-sendmail-envelope-from 'header)
;;(setq message-sendmail-extra-arguments '("--read-envelope-from"))
;;(setq sendmail-program "/usr/bin/msmtp")
;;(setq sendmail-program "/usr/sbin/sendmail")
;;(setq mu4e-html2text-command "html2text -utf8 -width 72")
(setq mu4e-html2text-command 'mu4e-shr2text)
(setq mu4e-headers-sort-field :date)
(setq mu4e-headers-sort-direction 'ascending)
(setq mu4e-headers-include-related t)
(setq mu4e-view-show-images t)
;;(setq mu4e-use-fancy-chars t)
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

(setq mu4e-org-contacts-file "~/doc/org/contacts.org")
(add-to-list 'mu4e-headers-actions
	     '("org-contact-add" . mu4e-action-add-org-contact) t)
(add-to-list 'mu4e-view-actions
	     '("org-contact-add" . mu4e-action-add-org-contact) t)
(add-to-list 'mu4e-view-actions
             '("ViewInBrowser" . mu4e-action-view-in-browser) t)

(setq mu4e-user-mail-address-list '("j@mesrichardson.com" "james@jamestechnotes.com" "jamesrichardson@riseup.net" "james.richardson@lowes.com"))
;; account specific stuff
(setq mu4e-sent-folder "/acct-4/INBOX.Sent"
      mu4e-drafts-folder "/acct-4/INBOX.Drafts"
      user-mail-address "j@mesrichardson.com")

(defvar my-mu4e-account-alist
  '(("acct-8"
     (mu4e-sent-folder "/acct-8/Sent")
     (mu4e-drafts-folder "/acct-8/Drafts")
     (user-mail-address "james.richardson@lowes.com"))
    ("acct-7"
     (mu4e-sent-folder "/acct-7/SentMail")
     (mu4e-drafts-folder "/acct-7/Drafts")
     (user-mail-address "jamesrichardsonjr@twc.com"))
    ("acct-6"
     (mu4e-sent-folder "/acct-6/INBOX.Sent")
     (mu4e-drafts-folder "/acct-6/INBOX.Drafts")
     (user-mail-address "j@mesrichardson.com"))
    ("acct-5"
     (mu4e-sent-folder "/acct-4/Sent")
     (mu4e-drafts-folder "/acct-4/Drafts")
     (user-mail-address "jamesrichardson@riseup.net"))
    ("acct-4"
     (mu4e-sent-folder "/acct-4/INBOX.Sent")
     (mu4e-drafts-folder "/acct-4/INBOX.Drafts")
     (user-mail-address "j@mesrichardson.com"))
    ("acct-3"
     (mu4e-sent-folder "/acct-3/INBOX.Sent")
     (mu4e-drafts-folder "/acct-3/INBOX.Drafts")
     (user-mail-address "james.richardson.jr@gmail.com"))
    ("acct-1"
     (mu4e-sent-folder "/acct-1/Sent")
     (mu4e-drafts-folder "/acct-1/Drafts")
     (user-mail-address "james.richardson@lowes.com"))))

(defun my-mu4e-set-account()
  "Set the account for composing a message."
  (let* ((account
	  (if mu4e-compose-parent-message
	      (let ((maildir (mu4e-message-field mu4e-compose-parent-message :maildir)))
		(string-match "/\\(.*?\\)/" maildir)
		(match-string 1 maildir))
	    (completing-read (format "Compose with account: (%s) "
				     (mapconcat #'(lambda (var) (car var))
						my-mu4e-account-alist "/"))
			     (mapcar #'(lambda (var) (car var)) my-mu4e-account-alist)
			     nil t nil nil (caar my-mu4e-account-alist))))
	 (account-vars (cdr (assoc account my-mu4e-account-alist))))
    (if account-vars
	(mapc #'(lambda (var) (set (car var) (cadr var)))
	      account-vars)
      (error "No email account found"))))

;;(add-hook 'mu4e-compose-pre-hook 'my-mu4e-set-account)
		  
	    
;; add hooks for epa-mail-mode
;;(add-hook 'mu4e-compose-mode-hook
;;	  (defun my-setup-epa-hook ()
;;	    (epa-mail-mode)))
;;(add-hook 'mu4e-view-mode-hook
;;  (defun my-view-mode-hook ()
;;   (epa-mail-mode)))

;; Setup Contexts
(setq mu4e-contexts
      `(,(make-mu4e-context
	  :name "Personal"
	  :enter-func (lambda () (mu4e-message "Entering Personal context"))
	  :leave-func (lambda () (mu4e-message "Leaving Personal context"))
	  :vars '((user-mail-address . "j@mesrichardson.com")
		  (mu4e-sent-folder . "/acct-6/INBOX.Sent")
		  (mu4e-drafts-folder . "/acct-6/INBOX.Drafts")))
	,(make-mu4e-context
	  :name "Technotes"
	  :vars '((user-mail-address . "james@jamestechnotes.com")
		  (mu4e-sent-folder . "/acct-6/INBOX.Sent")
		  (mu4e-drafts-folder . "/acct-6/INBOX.Drafts")))
	,(make-mu4e-context
	  :name "Lowes"
	  :vars '((user-mail-address . "james.richardson@lowes.com")
		  (mu4e-sent-folder . "/acct-8/Sent")
		  (mu4e-drafts-folder . "/acct-8/Drafts")))))

(setq mu4e-context-policy nil
      mu4e-compose-context-policy 'ask-if-none)
;; let's try to archive into year folders
(setq mu4e-refile-folder
      (lambda (msg)
	"Set folder to refile"
	(let ((archive)
	      (case-fold-search t)
	      (maildir (mu4e-message-field msg :maildir))
	      (year (format-time-string "%Y" (mu4e-message-field msg :date)))
	      (yearweek (format-time-string "%Y%U" (mu4e-message-field msg :date))))
	  (cond
	   ;; don't refile trash
	   ((string-match-p "trash" maildir) maildir)
	   ;; if /jrichar-lowes refile to /lowes-archive/YYYY
	   ((string-match-p "^/acct-1" maildir) (concat "/archives/spec01/mail." year)) ;; merged acct-1 and acct-8
	   ((string-match-p "^/acct-8" maildir) (concat "/archives/spec01/mail." year))
	   ((string-match-p "^/acct-[2-7]" maildir) (concat "/archives/mail." year))
	   ;; / is local
	   ((string-match-p "^/$" maildir) (concat "/archives/mail." year))
	   ;; default goes back to itself
	   (t maildir)))))


;; set bookmarks
(add-to-list 'mu4e-bookmarks
	     '("m:/acct-8/INBOX" "Work" ?W))
(add-to-list 'mu4e-bookmarks
	     '("flag:unread and not flag:trashed and not flag:list" "New (no lists)" ?n))
(add-to-list 'mu4e-bookmarks
	     '("flag:unread and not flag:trashed and not flag:list or m:/acct-8/INBOX or m:/acct-6/INBOX or m:/acct-6/Queue" "Incoming" ?i))
;;(add-to-list 'mu4e-bookmarks
;;	     '("(7.1 or aix) and date:1y..now"           "7.1"      ?7))
;; (add-to-list 'mu4e-bookmarks
;; 	     '("flag:list and not flag:trashed"  "Mailing lists"  ?l))
;; (add-to-list 'mu4e-bookmarks
;; 	     '("not flag:trashed and list:debian-user.lists.debian.org" "Debian-user" ?d))
;; (add-to-list 'mu4e-bookmarks
;; 	     '("flag:unread and not flag:trashed and (m:/ or m:/acct-1/INBOX or m:/acct-2/INBOX or m:/acct-2/acct-3.INBOX or m:/acct-2/acct-3.[Gmail].Important or m:/acct-2/acct-4/INBOX.james or m:/acct-3/INBOX or m:/acct-3/[Gmail].Important or m:/acct-4/INBOX.james)"    "New (all)"      ?N))
;; (add-to-list 'mu4e-bookmarks
;; 	     '("flag:unread and not flag:trashed and (m:/ or m:/acct-1/* or m:/acct-1-archives/*)"    "New"      ?n))
;; (add-to-list 'mu4e-bookmarks
;; 	     '("m:/ or m:/acct-1/INBOX or m:/acct-2/INBOX or m:/acct-2/acct-3.INBOX or m:/acct-2/acct-3.[Gmail].Important or m:/acct-2/acct-4/INBOX.james or m:/acct-2/acct-4.INBOX.Archive or m:/acct-3/INBOX or m:/acct-3/[Gmail].Important or m:/acct-4/INBOX.james or m:/acct-4/INBOX.Archive"  "Inbox (All)" ?I))
;; (add-to-list 'mu4e-bookmarks
;; 	     '("m:/ or m:/acct-1/INBOX" "Inbox" ?i))

;; set unread mail on desktop
(require 'mu4e-alert)
(mu4e-alert-set-default-style 'libnotify)
(add-hook 'after-init-hook #'mu4e-alert-enable-notifications)

;; set unread mail in mode line
(add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display)

;; update mail 300 seconds
(setq mu4e-update-interval 300)


;; start mu4e
(mu4e t)
