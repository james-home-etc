(when (require 'slime-autoloads nil 'noerror)
  (slime-setup '(slime-fancy))

  (eval-after-load 'slime
    `(define-key slime-prefix-map (kbd "M-h") 'slime-documentation-lookup))

  (setq slime-lisp-implementations
        '((sbcl ("sbcl"))
          (clisp ("clisp" "-ansi" "-q"))))

  (setq slime-default-lisp
        (cond
         ((string-equal system-type "cygwin") 'clisp)
         (t 'sbcl))))
