;;; main config
(transient-mark-mode t)
(global-set-key [remap kill-ring-save]
                (lambda (beg end flash);'jr/kill-ring-save)
                  (interactive (if (use-region-p)
                                   (list (region-beginning) (region-end) nil)
                                 (list (line-beginning-position)
                                       (line-beginning-position 2) 'flash)))
                  (kill-ring-save beg end)
                  (when flash
                    (save-excursion
                      (if (equal (current-column) 0)
                          (goto-char end)
                        (goto-char beg))
                      (sit-for blink-matching-delay)))))
                                         
(put 'kill-region 'interactive-form
     '(interactive
       (if (use-region-p)
           (list (region-beginning) (region-end))
         (list (line-beginning-position) (line-beginning-position 2)))))
;; whole-line-copy ends here
;; [[file:~/.emacs.d/emacs-init.org::*Searching%20and%20Selection][cua]]
(setq cua-enable-cua-keys 'shift)
(cua-mode t)
;; cua ends here
;; [[file:~/.emacs.d/emacs-init.org::*Searching%20and%20Selection][selection]]
(delete-selection-mode t)
(setq x-select-enable-clipboard t
      interprogram-paste-function 'x-cut-buffer-or-selection-value)
;; selection ends here
;; [[file:~/.emacs.d/emacs-init.org::*Searching%20and%20Selection][ido]]
(setq search-highlight t
      query-replace-highlight t
      completion-ignore-case t
      read-file-name-completion-ignore-case t
      read-buffer-completion-ignore-case t
      read-file-name-completion-ignore-case t)
;; Trying icicles. Remove this later.
;; (when (require 'ido nil 'noerror)
;;   (progn
;;     (ido-mode)
;;     (setq
;;      ido-mode 'both
;;      ido-everywhere t
;;      ido-max-directory-size 100000)))
;; (add-hook 'ido-minibuffer-setup-hook
;;           (function
;;            (lambda ()
;;              (make-local-variable 'resize-minibuffer-window-max-height)
;;              (setq resize-minibuffer-window-max-height .9))))
;; ;; ido ends here
;; [[file:~/.emacs.d/emacs-init.org::*Minibuffer][mini-buffer]]
;; Icicles
;; (require 'icicles)
;; (icy-mode 1)
;; (setq icicle-show-Completions-initially-flag nil
;;       icicle-incremental-completion 1
;;       icicle-top-level-when-sole-completion-flag t
;;       icicle-default-value t)


(setq enable-recursive-minibuffers nil
      max-mini-window-height .25
      minibuffer-scroll-window nil
      resize-mini-windows nil)

(icomplete-mode t)
(setq  icomplete-prospects-height 1
       icomplete-compute-delay 0)
;; mini-buffer ends here
;; [[file:~/.emacs.d/emacs-init.org::*Highlight%20line][hl-line-mode]]
(when (fboundp 'global-hl-line-mode)
  (setq global-hl-line-sticky-flag t)
  (global-hl-line-mode t))
;; hl-line-mode ends here
;; [[file:~/.emacs.d/emacs-init.org::*Show%20parenthesis][paren-mode]]
(when (fboundp 'show-paren-mode)
  (show-paren-mode t)
  (setq show-paren-style 'parenthesis))
;; paren-mode ends here
;; [[file:~/.emacs.d/emacs-init.org::*Misc][randome]]
;; Turn on timestamps
(setq 
 time-stamp-active t          ; do enable time-stamps
 time-stamp-line-limit 10     ; check first 10 buffer lines for Time-stamp: 
 time-stamp-format "%04y-%02m-%02d %02H:%02M:%02S %Z (%u@%h)") ; date format
(add-hook 'before-save-hook 'time-stamp) ; update when saving.
  
;; Configure midnight... 
;; I wish to run this every 4 hours... My laptop sleeps from time to time
;; and I don't want to worry about this not running. I primarily use to kill 
;; old buffers. I rarely close emacs.
(require 'midnight)
(midnight-delay-set 'midnight-delay 0)
(setq midnight-period 14400) ;; (eq (* 4 60 60) "4 hours")
  
;; tramp
;; doesn't work well with pinentry
;; (when (require 'tramp nil 'noerror)
;;   (setq tramp-default-method "ssh")
;;   (add-to-list 'tramp-default-proxies-alist
;;                '("isp[12]\\." nil "/ssh:s0998jrx@gls1:")))
(when (require 'tramp nil 'noerror)
  (setq tramp-default-method "ssh")
  ;; For guix hosts...:
  (add-to-list 'tramp-remote-path '"~/.config/guix/current/bin")
  (add-to-list 'tramp-remote-path "~/.guix-profile/bin")
  (add-to-list 'tramp-remote-path "/run/current-system/profile/bin"))

;; Define browser
(setq browse-url-generic-program
      (cond ((executable-find "icecat") "icecat")
            ((executable-find "firefox") "firefox"))
      brose-url-browser-function 'browse-url-generic)
            
;; modes
(require 'flyspell)
(when (executable-find ispell-program-name)
  (add-hook 'text-mode-hook 'turn-on-flyspell 'append))
(add-hook 'text-mode-hook 'turn-on-auto-fill 'append)
(autoload 'markdown-mode "markdown-mode.el" "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.mdwn$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.mkd$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(setq markdown-command "pandoc --number-sections --toc")

(add-hook 'tex-mode-hook (lambda () (setq ispell-parser 'tex)))

; cperl
(mapc
 (lambda (pair)
   (if (eq (cdr pair) 'perl-mode)
       (setcdr pair 'cperl-mode)))
 (append auto-mode-alist interpreter-mode-alist))
(setq cperl-electric-keywords t)
;;(setq cperl-hairy t)
(setq cperl-auto-newline t)
(setq cperl-extra-newline-before-brace t)
(setq cperl-extra-newline-before-brace-multiline t)
(setq cperl-highlight-variables-indiscriminately t)

;;git setup
(require 'vc-git)
(when (featurep 'vc-git) (add-to-list 'vc-handled-backends 'git))
;; allow emacs to start if git.el is not present on system
(when (require 'git nil 'noerror)
  (require 'git-blame)
  (autoload 'git-status "git" "Entry point into git-stats mode." t)
  (autoload 'git-blame-mode "git-blame" "Minor mode for incremental blame for Git." t))
  
  
  
;;(fset 'yes-or-no-p 'y-or-n-p)
  
  
(put 'narrow-to-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
(file-name-shadow-mode t)

;; restclient
(require 'restclient nil 'noerror)

;;elDoc
(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'eldoc-mode)
(add-hook 'ielm-mode-hook 'eldoc-mode)

(require 'eldoc)
(eldoc-add-command
 'paredit-backward-delete
 'paredit-close-round)


;; paredit
(when (require 'paredit nil 'noerror)
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
  (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
  (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
  (add-hook 'scheme-mode-hook           #'enable-paredit-mode)
  (add-hook 'clojure-mode-hook #'enable-paredit-mode)
  (add-hook 'cider-repl-mode-hook #'enable-paredit-mode))

;; company-mode
(when (fboundp 'global-company-mode)
  (add-hook 'after-init-hook 'global-company-mode))

;; start server
(require 'server)
(server-start)
;; edit with emacs
(when (locate-library "edit-server")
  (require 'edit-server)
  (edit-server-start t))
(autoload 'edit-server-maybe-dehtmlize-buffer "edit-server-htmlize" "edit-server-htmlize" t)
(autoload 'edit-server-maybe-htmlize-buffer "edit-server-htmlize" "edit-server-htmlize" t)
(add-hook 'edit-server-start-hook 'edit-server-maybe-dehtmlize-buffer)
(add-hook 'edit-server-done-hook 'edit-server-maybe-htmlize-buffer)

;; mode line
(setq display-time-24hr-format t
      display-time-mail-file t)
(display-time-mode t)
(display-battery-mode t)

;; I hate tabs
(setq-default indent-tabs-mode nil)

(defun compile-parent (command)
  (interactive
   (let* ((make-directory (locate-dominating-file (buffer-file-name)
                                                  "Makefile"))
          (command (concat "make -k -j -C "
                           (shell-quote-argument make-directory))))
     (list (compilation-read-command command))))
  (compile command))

;; Setup for Guix hacking
(with-eval-after-load 'yasnippet
  (add-to-list 'yas-snippet-dirs "~/src/guix/etc/snippets"))
