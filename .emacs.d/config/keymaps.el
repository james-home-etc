;; keymaps
(global-set-key (kbd "<f9> I") 
                (lambda (arg)
                  "Start countinuous clocking and set the default task to the selected task. If no task is selected the set Organization task as the default task."
                  (interactive "p")
                  (setq jr/keep-clock-running t)
                  (let* ((default-task-id '09623acd-0b34-4744-b228-b85e7b50ff06)
                         (clock-in-default #'(lambda ()
                                               (org-with-point-at (org-id-find default-task-id 'marker)
                                                 (org-clock-in '(16))))))
                    (if (equal major-mode 'org-agenda-mode)
                        ;; in the agenda
                        (let* ((marker (org-get-at-bol 'org-hd-marker))
                               (tags (org-with-point-at marker (org-get-tags-at))))
                          (if (and (eq arg 4) tags)
                              (org-agenda-clock-in '(16))
                            (funcall clock-in-default)))
                      ;; not the agenda
                      (save-restriction
                        (widen)
                        (if (and (equal major-mode 'org-mode)
                                 (not (org-before-first-heading-p))
                                 (eq arg 4))
                            (org-clock-in '(16))
                          (funcall clock-in-default)))))))
(global-set-key (kbd "<f9> O")
                (lambda ()
                  (interactive)
                  (setq jr/keep-clock-running nil)
                  (when (org-clock-is-active)
                    (org-clock-out))
                  (org-agenda-remove-restriction-lock)))
(global-set-key (kbd "<f9> s") (lambda() (interactive) (switch-to-buffer "*scratch*")))
(global-set-key (kbd "<f9> x") 'jr/connections) ;publish to connections
(global-set-key (kbd "<S-f10>") 'menu-bar-mode)
(global-set-key "\C-xgs" 'git-status) ;keybinding for git

;; Let's put all global key binding here (if possbile)
;; as I don't always have everything everywhere (i.e. I don't have all my org
;; everywhere, but I still want C-a <pc> to bring up an agenda, otherwise I get 
;; confused.
;; org-mode keys
;; As all the global keys are here, I can find them easier.
;; I need to figure out how I can have org-capture everywhere and the thing
;; get consolidated everywhere without merge conflicts.
;; I really don't want a branch per box, perhap a .../org/host/refile or something
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(define-key global-map "\C-cb" 'org-iswitchb)
(define-key global-map "\C-cc" 'org-capture)
