;;; appearance
(setq inhibit-startup-screen t
      inhibit-startup-echo-area-message "james"
      initial-scratch-message ";; scratch buffer created -- happy hacking\n")
(menu-bar-mode -1)
(tool-bar-mode -1)
(mouse-avoidance-mode 'cat-and-mouse)
(fringe-mode '(1 . 0))
;;(setq initial-frame-alist '((fullscreen . maximized)
;;                            (width . 132) (height . 45)
;;                            (top . 0) (left . (- 120))
;;			    (left-fringe . 0) (right-fringe . 0)))
;;(redraw-frame (selected-frame))
;;(setq default-frame-alist '((width . 132) (height . 45)
 ;;                           (top . 0) (left . (- 120))
;;			    (left-fringe . 0) (right-fringe . 0)))
(setq-default
 frame-title-format
 '(:eval
   (format "%s@%s:%s"
           (or (file-remote-p default-directory 'user) user-login-name)
           (or (file-remote-p default-directory 'host) system-name)
           (file-name-nondirectory (or (buffer-file-name) default-directory)))))
(line-number-mode t)
(column-number-mode t)
(size-indication-mode t)
(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-up-aggressivel nil
      scroll-down-aggressively nil
      scroll-preserve-screen-position t)
