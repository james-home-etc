

;;; load-paths.el --- configure load paths

;; If I track org, I need to add my org to the load path and then uninstall the shipping org-mode
(when (file-directory-p "~/src/org-mode")
  (add-to-list 'load-path (file-truename "~/src/org-mode/contrib/lisp"))
  (add-to-list 'load-path (file-truename "~/src/org-mode/lisp"))
  (mapc
   (lambda (x)
     (let ((feat (cdr (assoc 'provide (cdr x)))))
       (and (string-match "^org\\|^ox\\|^ob" (symbol-name feat))
	    (featurep feat)
	    (unload-feature feat t))))
   load-history))

(let ((host-site-lisp (concat "~/.emacs.d/site-lisp-" (system-name))))
  (when (file-directory-p host-site-lisp)
    (add-to-list 'load-path (file-truename host-site-lisp))))

(let ((site-lisp (concat "~/.emacs.d/site-lisp")))
  (when (file-directory-p site-lisp)
    (add-to-list 'load-path (file-truename site-lisp))))

;; blog-tools, which I'm current working on
;; use name-space jrblog
(when (file-directory-p "~/src/blog-tools")
  (add-to-list 'load-path (file-truename "~/src/blog-tools/lisp")))

;; restclient.el  git@github.com:pashky/restclient.el.git
(when (file-directory-p "~/src/restclient.el")
  (add-to-list 'load-path (file-truename "~/src/restclient.el")))

;; gitolite-conf mode
(when (file-directory-p "~/src/gitolite-emacs")
  (add-to-list 'load-path (file-truename "~/src/gitolite-emacs"))
  (require 'gl-conf-mode)
  (add-to-list 'auto-mode-alist '("gitolite\\.conf\\'" . gl-conf-mode)))

;;; org-weather
(when (file-directory-p "~/src/org-weather")
  (add-to-list 'load-path (file-truename "~/src/org-weather")))

;;; org-jira
(when (file-directory-p "~/src/org-jira")
  (add-to-list 'load-path (file-truename "~/src/org-jira")))

;;; emacs-request
(when (file-directory-p "~/src/emacs-request")
  (add-to-list 'load-path (file-truename "~/src/emacs-request")))
