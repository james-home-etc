umask 022

# This *has* to be done before keychain, at least on foreign distros
# set up Guix if we have such
export GUIX_PROFILE=$HOME/.config/guix/current
[ -e ${GUIX_PROFILE}/etc/profile ] && . ${GUIX_PROFILE}/etc/profile

# pull in keys, if installed
if [ -x "$(which keychain 2>/dev/null)" ]; then
  case $- in
    *i*) keychain --agents "gpg,ssh" --inherit any-once --ignore-missing id_rsa id_ed25519
  esac
  host=`uname -n`
  [ -f ~/.keychain/$host-sh ] && . ~/.keychain/$host-sh
  [ -f ~/.keychain/$host-sh-gpg ] && . ~/.keychain/$host-sh-gpg
fi
# source environment from .profile not form .rc
. $HOME/.environment
# Oh joy, lets find out what shell we are in.
# At the moment, it should only be bash or korn.
# Bash is easy
if [ "$BASH" ]; then
  . $HOME/.bashrc
else
  case $SHELL in
    *ksh) ENV=$HOME/.kshrc; export ENV
          #[ X"/usr/bin/ksh" == X"${SHELL}" ] && [ -x /bin/bash ] && exec SHELL=/bin/bash /bin/bash -i
          ;;
    *) # unknown shell, source .environment and hope for the best
       #. $HOME/.environment
       ;;
  esac
fi
